/*
 * Copyright (C) 2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright 2020 congatec AG
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <linux/errno.h>
#include <asm/mach-imx/video.h>
#include "../../../board/congatec/cgtsx8m/video_params_cgtsx8m.h"

typedef struct {
	char * modename;
	char * modestring;
} sx8m_video_mode_t;

sx8m_video_mode_t sx8m_video_modes[] =
{
	{   "640x480", "sx8m:x:640,y:480,depth:24,pclk:39721,hbp:40,hfp:24,vbp:32,vfp:11,hsw:96,vsw:2"    },
	{   "800x600", "sx8m:x:800,y:600,depth:24,pclk:25000,hbp:88,hfp:40,vbp:23,vfp:1,hsw:128,vsw:4"    },
	{  "1024x768", "sx8m:x:1024,y:768,depth:24,pclk:15384,hbp:160,hfp:24,vbp:29,vfp:3,hsw:136,vsw:6"  },
	{  "1152x864", "sx8m:x:1152,y:864,depth:24,pclk:12251,hbp:184,hfp:64,vbp:27,vfp:1,hsw:120,vsw:3"  },
	{  "1280x720", "sx8m:x:1280,y:720,depth:24,pclk:13500,hbp:220,hfp:110,vbp:20,vfp:5,hsw:40,vsw:5"  },
	{  "1280x800", "sx8m:x:1280,y:800,depth:24,pclk:11981,hbp:200,hfp:64,vbp:24,vfp:1,hsw:136,vsw:3"  },
	{  "1440x900", "sx8m:x:1440,y:900,depth:24,pclk:9392,hbp:232,hfp:80,vbp:28,vfp:1,hsw:152,vsw:3"   },
	{  "1368x768", "sx8m:x:1368,y:768,depth:24,pclk:11646,hbp:216,hfp:72,vbp:23,vfp:1,hsw:144,vsw:3"   },
	{ "1280x1024", "sx8m:x:1280,y:1024,depth:24,pclk:9090,hbp:200,hfp:48,vbp:26,vfp:1,hsw:184,vsw:3"  },
	{ "1680x1050", "sx8m:x:1680,y:1050,depth:24,pclk:8387,hbp:80,hfp:48,vbp:21,vfp:3,hsw:32,vsw:6" },
	{ "1600x1200", "sx8m:x:1600,y:1200,depth:24,pclk:6172,hbp:304,hfp:64,vbp:46,vfp:1,hsw:192,vsw:3" },
	{ "1920x1080", "sx8m:x:1920,y:1080,depth:24,pclk:6734,hbp:148,hfp:88,vbp:36,vfp:4,hsw:44,vsw:5" },
};

// -1 .. error reading integer
//  0 .. parameter not found
//  1 .. parameter found and read correctly

int get_int(char const * * parString, char const * name, u32 * var)
{
	char * next_par_p;

	if ( strncmp(*parString, name, strlen(name)) == 0 )
	{
		* var = simple_strtoul((*parString) + strlen(name), &next_par_p, 10);
		if ( ((*parString) + strlen(name)) == next_par_p )
		{
			printf("Error reading value of %s\n", name);
			return -1;
		}
		else
		{
			*parString = next_par_p;
			while ( ( **parString != '\0' ) &&
				( ( **parString == ' ' ) ||
				  ( **parString == ',' )
				)
			      )
			{
				*parString += 1;
			}
			return 1;
		}
	}

	return 0;
}

//  0 .. parameter not found
//  1 .. parameter found

int get_bool(char const * * parString, char const * name)
{
	if ( strncmp(*parString, name, strlen(name)) == 0 )
	{
		* parString = (* parString) + strlen(name);
		while ( ( **parString != '\0' ) &&
			( ( **parString == ' ' ) ||
			  ( **parString == ':' ) ||
			  ( **parString == ',' )
			)
		      )
		{
			*parString += 1;
		}
		return 1;
	}
	return 0;
}

// these are mandatory
#define FOUND_X		1
#define FOUND_Y		2
#define FOUND_BPP	4
#define FOUND_PCLK	8
#define FOUND_HBP	16
#define FOUND_HFP	32
#define FOUND_VBP	64
#define FOUND_VFP	128
#define FOUND_HSW	256
#define FOUND_VSW	512

#define FOUND_MANDATORY 0x3FF // all the above

int set_display(char const * modename, char const * modestring, struct display_info_t * display)
{
	int foundNext = 0;
	unsigned int foundMap = 0;
	struct fb_videomode vmode = {0};
	unsigned int bpp = 0;

	if (!get_bool(&modestring, "sx8m"))
	{
		printf("Incorrect modestring start\n");
		return -1;
	}

	vmode.name = modename;

	do {
		foundNext = 0;

		if ( get_int( &modestring, "x:", &vmode.xres ) )
		{
			foundMap |= FOUND_X;
			foundNext = 1;
		}

		if ( get_int( &modestring, "y:", &vmode.yres ) )
		{
			foundMap |= FOUND_Y;
			foundNext = 1;
		}

		if ( get_int( &modestring, "depth:", &bpp ) )
		{
			foundMap |= FOUND_BPP;
			foundNext = 1;
		}

		if ( get_int( &modestring, "pclk:", &vmode.pixclock ) )
		{
			foundMap |= FOUND_PCLK;
			foundNext = 1;
		}

		if ( get_int( &modestring, "hbp:", &vmode.left_margin ) )
		{
			foundMap |= FOUND_HBP;
			foundNext = 1;
		}

		if ( get_int( &modestring, "hfp:", &vmode.right_margin ) )
		{
			foundMap |= FOUND_HFP;
			foundNext = 1;
		}

		if ( get_int( &modestring, "vbp:", &vmode.upper_margin ) )
		{
			foundMap |= FOUND_VBP;
			foundNext = 1;
		}

		if ( get_int( &modestring, "vfp:", &vmode.lower_margin ) )
		{
			foundMap |= FOUND_VFP;
			foundNext = 1;
		}

		if ( get_int( &modestring, "hsw:", &vmode.hsync_len ) )
		{
			foundMap |= FOUND_HSW;
			foundNext = 1;
		}

		if ( get_int( &modestring, "vsw:", &vmode.vsync_len ) )
		{
			foundMap |= FOUND_VSW;
			foundNext = 1;
		}

	} while ( foundNext );

	if ( *modestring != '\0' )
	{
		printf("Unparsed characters (%s) ... cannot use mode\n", modestring);
		return -1;
	}

	if ((foundMap & 0x3ff) != 0x3ff )
	{
		printf("Not all mandatory parameters defined (0x%x)\n", foundMap);
		return -1;
	}

	vmode.sync = display->mode.sync;
	vmode.vmode = display->mode.vmode;

	if ( bpp == 18 )
	{
	  vmode.vmode |= FB_VMODE_BRIDGE_BPP_IS_18_MASK;
	}
	else if ( bpp != 24 )
	{
	  printf("Bridges support only 18 or 24 BPP\n");
	  return -1;
	}

	// at this point we have all mandatory parameters, let's look if there is some 'tuning'
	char const *videoTuning = env_get("video_tuning");

	if ( ! videoTuning )
	{
		printf("video_tuning variable not found, using defaults\n");
	}
	else
	{
		do {

			foundNext = 0;

			if (get_bool(&videoTuning, "hsync-low"))
			{
				vmode.sync |= FB_SYNC_HSYNC_ACTIVE_LOW_MASK;
				foundNext = 1;
				printf("Hsync is active low\n");
			}

			if (get_bool(&videoTuning, "vsync-low"))
			{
				vmode.sync |= FB_SYNC_VSYNC_ACTIVE_LOW_MASK;
				foundNext = 1;
				printf("Vsync is active low\n");
			}

			if (get_bool(&videoTuning, "de-low"))
			{
				vmode.sync |= FB_SYNC_DE_ACTIVE_LOW_MASK;
				foundNext = 1;
				printf("Data enable is active low\n");
			}

			if (get_bool(&videoTuning, "lvds-use-jeida"))
			{
				vmode.vmode |= FB_VMODE_LVDS_BRIDGE_BPPF_IS_JEIDA_MASK;
				foundNext = 1;
				printf("LVDS uses JEIDA\n");
			}

			if (get_bool(&videoTuning, "lvds-is-two-channel"))
			{
				vmode.vmode |= FB_VMODE_LVDS_IS_TWO_CHANNEL_MASK;
				foundNext = 1;
				printf("LVDS is two-channel\n");
			}

		} while ( foundNext );

		if ( *videoTuning != '\0' )
		{
			printf("Unparsed characters in tuning string (%s) ... continue anyway\n", videoTuning);
		}
	}

	printf("\"video_tuning\" available options: hsync-low, vsync-low, de-low, lvds-use-jeida, lvds-is-two-channel; separate with \":\"\n");

	display->mode = vmode;

	return 0;
}


int board_video_skip(void)
{
	int i;
	int ret = 0;
	char const *panel = env_get("panel");
	char const *custom_mode = env_get("custom_video_mode");

	for (int vi=0; vi < sizeof(sx8m_video_modes) / sizeof(sx8m_video_mode_t); vi++)
	{
		printf("Panel: %s, modestring %s\n", sx8m_video_modes[vi].modename, sx8m_video_modes[vi].modestring);
	}

	if ( !custom_mode )
	{
		printf("\"custom_video_mode\" variable is not set\n");
	}
	else
	{
		printf("Panel: \"custom_video_mode\", modestring: %s\n", custom_mode);
	}

	if (!panel)
	{
		panel = "1024x768";
		printf("No panel defined, using %s as the default\n", panel);
	}

	int panelFound = 0;

	if ( custom_mode && ( strcmp(panel, "custom_video_mode") == 0 ) )
	{
		panelFound = 1;

		if (set_display("custom_video_mode", custom_mode, &displays[0]) == 0)
		{
			i = 0;
		}
		else
		{
			i = display_count;
		}
	}

	for (int vi=0; ((vi<sizeof(sx8m_video_modes)/sizeof(sx8m_video_mode_t)) && ( ! panelFound )); vi++)
	{
		if ( strcmp(panel, sx8m_video_modes[vi].modename) == 0 )
		{
			panelFound = 1;

			if (set_display(sx8m_video_modes[vi].modename, sx8m_video_modes[vi].modestring, &displays[0]) == 0)
			{
				i = 0;
			}
			else
			{
				i = display_count;
			}
		}
	}

	if ( ! panelFound )
	{
		i = display_count;
	}

	/* if (!panel) {
		 for (i = 0; i < display_count; i++) {
			struct display_info_t const *dev = displays+i;
			if (dev->detect && dev->detect(dev)) {
				panel = dev->mode.name;
				printf("auto-detected panel %s\n", panel);
				break;
			}
		}
		if (!panel) {
			panel = displays[0].mode.name;
			printf("No panel detected: default to %s\n", panel);
			i = 0;
		}
	} else {
		for (i = 0; i < display_count; i++) {
			if (!strcmp(panel, displays[i].mode.name))
				break;
		}
	}*/

	if (i < display_count) {
#if defined(CONFIG_VIDEO_IPUV3)
		ret = ipuv3_fb_init(&displays[i].mode, displays[i].di ? 1 : 0,
				    displays[i].pixfmt);
#elif defined(CONFIG_VIDEO_IMXDPUV1)
		ret = imxdpuv1_fb_init(&displays[i].mode, displays[i].bus,
					displays[i].pixfmt);
#elif defined(CONFIG_VIDEO_IMXDCSS)
		ret = imx8m_fb_init(&displays[i].mode, displays[i].bus,
					displays[i].pixfmt);
#elif defined(CONFIG_VIDEO_MXS)
		ret = mxs_lcd_panel_setup(displays[i].mode,
					displays[i].pixfmt,
				    displays[i].bus);
#endif
		if (!ret) {
			if (displays[i].enable)
				displays[i].enable(displays + i);

			printf("Display: %s (%ux%u)\n",
			       displays[i].mode.name,
			       displays[i].mode.xres,
			       displays[i].mode.yres);
		} else
			printf("LCD %s cannot be configured: %d\n",
			       displays[i].mode.name, ret);
	} else {
		if (strcmp(panel, "NULL"))
			printf("unsupported panel %s\n", panel);
		return -EINVAL;
	}

	return ret;
}

#ifdef CONFIG_IMX_HDMI
#include <asm/arch/mxc_hdmi.h>
#include <asm/io.h>
int detect_hdmi(struct display_info_t const *dev)
{
	struct hdmi_regs *hdmi	= (struct hdmi_regs *)HDMI_ARB_BASE_ADDR;
	return readb(&hdmi->phy_stat0) & HDMI_DVI_STAT;
}
#endif
