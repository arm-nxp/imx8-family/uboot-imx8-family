/*
 * Copyright 2018 NXP
 *
 * SPDX-License-Identifier: GPL-2.0+
 * Common file for ddr code
 */

#ifndef __IMX8M_DDR_H__
#define __IMX8M_DDR_H__

#include <asm/io.h>
#include <asm/types.h>
#include <asm/arch/ddr.h>

#define	ADDR_START_1G	0x40000000
#define	ADDR_START_2G	0x80000000
#define	ADDR_START_3G	0xC0000000
#define	ADDR_START_4G	0x100000000
#define	ADDR_START_5G	0x140000000

/* user data type */
typedef enum mVariant_e {
	lpddr_1G,
	lpddr_2G,
	lpddr_4G,
	lpddr_invalid
} mVariant_t;

enum fw_type {
	FW_1D_IMAGE,
	FW_2D_IMAGE,
};

struct dram_cfg_param {
	unsigned int reg;
	unsigned int val;
};

struct dram_fsp_msg {
	unsigned int drate;
	enum fw_type fw_type;
	struct dram_cfg_param *fsp_cfg;
	unsigned int fsp_cfg_num;
};

struct dram_timing_info {
	/* umctl2 config */
	struct dram_cfg_param *ddrc_cfg;
	unsigned int ddrc_cfg_num;
	/* ddrphy config */
	struct dram_cfg_param *ddrphy_cfg;
	unsigned int ddrphy_cfg_num;
	/* ddr fsp train info */
	struct dram_fsp_msg *fsp_msg;
	unsigned int fsp_msg_num;
	/* ddr phy trained CSR */
	struct dram_cfg_param *ddrphy_trained_csr;
	unsigned int ddrphy_trained_csr_num;
	/* ddr phy PIE */
	struct dram_cfg_param *ddrphy_pie;
	unsigned int ddrphy_pie_num;
	/* initialized drate table */
	unsigned int fsp_table[4];
};

extern struct dram_timing_info dram_timing_1G;
extern struct dram_timing_info dram_timing_2G;
extern struct dram_timing_info dram_timing_4G;

void ddr_load_train_firmware(enum fw_type type);
int ddr_init(struct dram_timing_info *timing_info);
int ddr_cfg_phy(struct dram_timing_info *timing_info);
void load_lpddr4_phy_pie(void);
void ddrphy_trained_csr_save(struct dram_cfg_param *, unsigned int);
void dram_config_save(struct dram_timing_info *, unsigned long);
mVariant_t get_memory_variant(void);
phys_size_t detect_phys_ram_size(void);
phys_size_t get_phys_ram_size(void);

/* utils function for ddr phy training */
int wait_ddrphy_training_complete(void);
void ddrphy_init_set_dfi_clk(unsigned int drate);
void ddrphy_init_read_msg_block(enum fw_type type);

static inline void reg32_write(unsigned long addr, u32 val)
{
	writel(val, addr);
}

static inline u32 reg32_read(unsigned long addr)
{
	return readl(addr);
}

static inline void reg32setbit(unsigned long addr, u32 bit)
{
	setbits_le32(addr, (1 << bit));
}

#define dwc_ddrphy_apb_wr(addr, data)	reg32_write(IP2APB_DDRPHY_IPS_BASE_ADDR(0) + 4*(addr), data)
#define dwc_ddrphy_apb_rd(addr)		reg32_read(IP2APB_DDRPHY_IPS_BASE_ADDR(0) + 4*(addr))

#endif /* __IMX8M_DDR_H__ */
