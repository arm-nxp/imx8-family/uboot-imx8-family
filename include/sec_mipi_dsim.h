/*
 * Copyright 2018 NXP
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __SEC_MIPI_DSIM_H__
#define __SEC_MIPI_DSIM_H__

#include <imx_mipi_dsi_bridge.h>

struct sec_mipi_dsim_plat_data {
	uint32_t version;
	uint32_t max_data_lanes;
	uint64_t max_data_rate;
	ulong reg_base;
	ulong gpr_base;
};

struct sec_mipi_dsim {
	void __iomem *base;
	void __iomem *disp_mix_gpr_base;

	/* kHz clocks */
	uint64_t pix_clk;
	uint64_t bit_clk;

	unsigned int lanes;
	unsigned int channel;			/* virtual channel */
	enum mipi_dsi_pixel_format format;
	unsigned long mode_flags;
	unsigned int pms;
	unsigned int p;
	unsigned int m;
	unsigned int s;
	struct fb_videomode vmode;

	const struct sec_mipi_dsim_plat_data *pdata;

	struct mipi_dsi_client_dev *dsi_panel_dev;
	struct mipi_dsi_client_driver *dsi_panel_drv;
};

struct sec_mipi_dsim * sec_mipi_dsim_setup(const struct sec_mipi_dsim_plat_data *plat_data);
#endif
