/*
 * Copyright 2018-2019 NXP
 * Copyright 2020 congatec AG
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <spl.h>
#include <asm/io.h>
#include <errno.h>
#include <asm/io.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm/arch/imx8mm_pins.h>
#include <asm/arch/sys_proto.h>
#include <power/pmic.h>
#include <power/bd71837.h>
#include <asm/arch/clock.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <asm/arch/imx8m_ddr.h>
#include <asm/armv8/mmu.h>

DECLARE_GLOBAL_DATA_PTR;

#define	USB1_BASE	0x32E40000
#define USBNC_PHY_CFG_OFFSET	0x230

#define TXPREEMP_MASK	0x3
#define TXPREEMP_SHIFT	28

#define TXVREF_MASK 	0xF
#define TXVREF_SHIFT	20

#define PREEMP_CURR_DISABLE	0x0
#define PREEMP_CURR_1X		0x1
#define PREEMP_CURR_2X		0x2
#define PREEMP_CURR_3X		0x3

#define HS_DC_VOLT_MINUS_6	0x0
#define HS_DC_VOLT_MINUS_4	0x1
#define HS_DC_VOLT_MINUS_2	0x2
#define HS_DC_VOLT_DEFAULT	0x3
#define HS_DC_VOLT_PLUS_2	0x4
#define HS_DC_VOLT_PLUS_4	0x5
#define HS_DC_VOLT_PLUS_6	0x6
#define HS_DC_VOLT_PLUS_8	0x7
#define HS_DC_VOLT_PLUS_10	0x8
#define HS_DC_VOLT_PLUS_12	0x9
#define HS_DC_VOLT_PLUS_14	0xA
#define HS_DC_VOLT_PLUS_16	0xB
#define HS_DC_VOLT_PLUS_18	0xC
#define HS_DC_VOLT_PLUS_20	0xD
#define HS_DC_VOLT_PLUS_22	0xE
#define HS_DC_VOLT_PLUS_24	0xF

void poweron_and_tune_usb(void)
{
	out_le32( CCM_CCGR(77), 0x02 ); // usb bus clock on

	reg32setbit(0x303A00F8, 2); // USB OTG1 power on
	reg32setbit(0x303A00F8, 3); // USB OTG2 power on

	udelay(50);

	unsigned int phycfg_tmp = in_le32( USB1_BASE + USBNC_PHY_CFG_OFFSET );

	phycfg_tmp &= ~( TXPREEMP_MASK << TXPREEMP_SHIFT );
	phycfg_tmp &= ~( TXVREF_MASK << TXVREF_SHIFT );

	phycfg_tmp |= PREEMP_CURR_3X << TXPREEMP_SHIFT;
	phycfg_tmp |= HS_DC_VOLT_PLUS_12 << TXVREF_SHIFT;

	out_le32( USB1_BASE + USBNC_PHY_CFG_OFFSET, phycfg_tmp );
}

int spl_dram_init(mVariant_t mVariant)
{
	switch (mVariant)
	{
		case lpddr_4G:
			printf("Doing LPDDR init - 4G variant\n");
			return(ddr_init(&dram_timing_4G));
			break;
		case lpddr_2G:
			printf("Doing LPDDR init - 2G variant\n");
			return(ddr_init(&dram_timing_2G));
			break;
		case lpddr_1G:
			printf("Doing LPDDR init - 1G variant\n");
			return(ddr_init(&dram_timing_1G));
			break;
		default: return 0;
	}
}

#define I2C_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_HYS | PAD_CTL_PUE | PAD_CTL_PE)
#define PC MUX_PAD_CTRL(I2C_PAD_CTRL)
struct i2c_pads_info i2c_pad_info1 = {
	.scl = {
		.i2c_mode = IMX8MM_PAD_I2C1_SCL_I2C1_SCL | PC,
		.gpio_mode = IMX8MM_PAD_I2C1_SCL_GPIO5_IO14 | PC,
		.gp = IMX_GPIO_NR(5, 14),
	},
	.sda = {
		.i2c_mode = IMX8MM_PAD_I2C1_SDA_I2C1_SDA | PC,
		.gpio_mode = IMX8MM_PAD_I2C1_SDA_GPIO5_IO15 | PC,
		.gp = IMX_GPIO_NR(5, 15),
	},
};

#define USDHC2_CD_GPIO	IMX_GPIO_NR(2, 18)
#define USDHC2_PWR_GPIO IMX_GPIO_NR(2, 19)

#define USDHC_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_HYS | PAD_CTL_PUE |PAD_CTL_PE | \
			 PAD_CTL_FSEL2)
#define USDHC_GPIO_PAD_CTRL (PAD_CTL_HYS | PAD_CTL_DSE1)

static iomux_v3_cfg_t const usdhc3_pads[] = {
	IMX8MM_PAD_NAND_WE_B_USDHC3_CLK | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_WP_B_USDHC3_CMD | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA04_USDHC3_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA05_USDHC3_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA06_USDHC3_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA07_USDHC3_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_RE_B_USDHC3_DATA4 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_CE2_B_USDHC3_DATA5 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_CE3_B_USDHC3_DATA6 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_NAND_CLE_USDHC3_DATA7 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
};

static iomux_v3_cfg_t const usdhc2_pads[] = {
	IMX8MM_PAD_SD2_CLK_USDHC2_CLK | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_SD2_CMD_USDHC2_CMD | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_SD2_DATA0_USDHC2_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_SD2_DATA1_USDHC2_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_SD2_DATA2_USDHC2_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_SD2_DATA3_USDHC2_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL),
	IMX8MM_PAD_SD2_RESET_B_GPIO2_IO19 | MUX_PAD_CTRL(USDHC_GPIO_PAD_CTRL),
};

/*
 * The evk board uses DAT3 to detect CD card plugin,
 * in u-boot we mux the pin to GPIO when doing board_mmc_getcd.
 */
static iomux_v3_cfg_t const usdhc2_cd_pad =
	IMX8MM_PAD_SD2_DATA3_GPIO2_IO18 | MUX_PAD_CTRL(USDHC_GPIO_PAD_CTRL);

static iomux_v3_cfg_t const usdhc2_dat3_pad =
	IMX8MM_PAD_SD2_DATA3_USDHC2_DATA3 |
	MUX_PAD_CTRL(USDHC_PAD_CTRL);


static struct fsl_esdhc_cfg usdhc_cfg[2] = {
	{USDHC2_BASE_ADDR, 0, 1},
	{USDHC3_BASE_ADDR, 0, 1},
};

int board_mmc_init(bd_t *bis)
{
	int i, ret;
	/*
	 * According to the board_mmc_init() the following map is done:
	 * (U-Boot device node)    (Physical Port)
	 * mmc0                    USDHC1
	 * mmc1                    USDHC2
	 */
	for (i = 0; i < CONFIG_SYS_FSL_USDHC_NUM; i++) {
		switch (i) {
		case 0:
			usdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC2_CLK);
			imx_iomux_v3_setup_multiple_pads(
				usdhc2_pads, ARRAY_SIZE(usdhc2_pads));
			gpio_request(USDHC2_PWR_GPIO, "usdhc2_reset");
			gpio_direction_output(USDHC2_PWR_GPIO, 0);
			udelay(500);
			gpio_direction_output(USDHC2_PWR_GPIO, 1);
			break;
		case 1:
			usdhc_cfg[1].sdhc_clk = mxc_get_clock(MXC_ESDHC3_CLK);
			imx_iomux_v3_setup_multiple_pads(
				usdhc3_pads, ARRAY_SIZE(usdhc3_pads));
			break;
		default:
			printf("Warning: you configured more USDHC controllers"
				"(%d) than supported by the board\n", i + 1);
			return -EINVAL;
		}

		ret = fsl_esdhc_initialize(bis, &usdhc_cfg[i]);
		if (ret)
			return ret;
	}

	return 0;
}

int board_mmc_getcd(struct mmc *mmc)
{
	struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;
	int ret = 0;

	switch (cfg->esdhc_base) {
	case USDHC3_BASE_ADDR:
		ret = 1;
		break;
	case USDHC2_BASE_ADDR:
		imx_iomux_v3_setup_pad(usdhc2_cd_pad);
		gpio_request(USDHC2_CD_GPIO, "usdhc2 cd");
		gpio_direction_input(USDHC2_CD_GPIO);

		/*
		 * Since it is the DAT3 pin, this pin is pulled to
		 * low voltage if no card
		 */
		ret = gpio_get_value(USDHC2_CD_GPIO);

		imx_iomux_v3_setup_pad(usdhc2_dat3_pad);
		return ret;
	}

	return 1;
}

#ifdef CONFIG_POWER
#define I2C_PMIC	0
int power_init_board(void)
{
	struct pmic *p;
	int ret;

	ret = power_bd71837_init(I2C_PMIC);
	if (ret)
		printf("power init failed");

	p = pmic_get("BD71837");
	pmic_probe(p);


	/* decrease RESET key long push time from the default 10s to 10ms */
	pmic_reg_write(p, BD71837_PWRONCONFIG1, 0x0);

	/* unlock the PMIC regs */
	pmic_reg_write(p, BD71837_REGLOCK, 0x1);

	/* increase VDD_SOC to typical value 0.85v before first DRAM access */
	pmic_reg_write(p, BD71837_BUCK1_VOLT_RUN, 0x0f);

	/* increase VDD_DRAM to 0.975v for 3Ghz DDR */
	pmic_reg_write(p, BD71837_BUCK5_VOLT, 0x83);

	/* set BUCK2 for 1.2GHz operation to 0.85V */
	pmic_reg_write(p, BD71837_BUCK2_VOLT_RUN, 0x0f);

#ifndef CONFIG_IMX8M_LPDDR4
	/* increase NVCC_DRAM_1V2 to 1.2v for DDR4 */
	pmic_reg_write(p, BD71837_BUCK8_VOLT, 0x28);
#endif

	/* lock the PMIC regs */
	pmic_reg_write(p, BD71837_REGLOCK, 0x11);

	return 0;
}
#endif

void spl_board_init(void)
{
#ifndef CONFIG_SPL_USB_SDP_SUPPORT
	/* Serial download mode */
	if (is_usb_boot()) {
		puts("Back to ROM, SDP\n");
		restore_boot_params();
	}
#endif
	puts("Normal Boot\n");
}

#ifdef CONFIG_SPL_LOAD_FIT
int board_fit_config_name_match(const char *name)
{
	/* Just empty function now - can't decide what to choose */
	debug("%s: %s\n", __func__, name);

	return 0;
}
#endif

static void fspi_init(void)
{
	out_le32(0x30bb0000, 0x1); // issue SW reset

	do {  // wait for its completion
		udelay(1);
	} while (0x1 & in_le32(0x30bb0000));

	unsigned int temp = in_le32(0x30bb0000);  // disable the module
	out_le32(0x30bb0000, temp | 0x2);

	out_le32(0x30bb0000, 0xffff0000); // enable the module and set timeout wait for AHB/IP commands

	out_le32(0x30bb00c0, 0x100); // dll acr
	out_le32(0x30bb00c4, 0x100); // dll bcr

	out_le32(0x30bb0060, 0x2000); // Max flash size for A1 (in KiB), i.e. 8MiB

	out_le32(0x30bb0064, 0x0); // we use only single die flash device, so A2, B1, and B2 are 0
	out_le32(0x30bb0068, 0x0);
	out_le32(0x30bb006c, 0x0);

	out_le32(0x30bb0018, 0x5af05af0); // unlock LUT
	out_le32(0x30bb001c, 0x2);

	out_le32(0x30bb0200, 0x8200403);  // set the LUT
	out_le32(0x30bb0204, 0x2400);
	out_le32(0x30bb0208, 0x0);
	out_le32(0x30bb020c, 0x0);
	out_le32(0x30bb0210, 0x406);
	out_le32(0x30bb0214, 0x0);
	out_le32(0x30bb0218, 0x0);
	out_le32(0x30bb021c, 0x0);
	out_le32(0x30bb0220, 0x818040b);
	out_le32(0x30bb0224, 0x24003008);
	out_le32(0x30bb0228, 0x0);
	out_le32(0x30bb022c, 0x0);
	out_le32(0x30bb0230, 0x24010405);
	out_le32(0x30bb0234, 0x0);
	out_le32(0x30bb0238, 0x0);
	out_le32(0x30bb023c, 0x0);
	out_le32(0x30bb0240, 0x81804d8);
	out_le32(0x30bb0244, 0x0);
	out_le32(0x30bb0248, 0x0);
	out_le32(0x30bb024c, 0x0);
	out_le32(0x30bb0250, 0x4c7);
	out_le32(0x30bb0254, 0x0);
	out_le32(0x30bb0258, 0x0);
	out_le32(0x30bb025c, 0x0);
	out_le32(0x30bb0260, 0x8180402);
	out_le32(0x30bb0264, 0x2000);
	out_le32(0x30bb0268, 0x0);
	out_le32(0x30bb026c, 0x0);
	out_le32(0x30bb0270, 0x2408049f);
	out_le32(0x30bb0274, 0x0);
	out_le32(0x30bb0278, 0x0);
	out_le32(0x30bb027c, 0x0);
	out_le32(0x30bb0280, 0x8180420);
	out_le32(0x30bb0284, 0x0);
	out_le32(0x30bb0288, 0x0);
	out_le32(0x30bb028c, 0x0);
	out_le32(0x30bb0290, 0x24010416);
	out_le32(0x30bb0294, 0x0);
	out_le32(0x30bb0298, 0x0);
	out_le32(0x30bb029c, 0x0);
	out_le32(0x30bb02a0, 0x20010417);
	out_le32(0x30bb02a4, 0x0);
	out_le32(0x30bb02a8, 0x0);
	out_le32(0x30bb02ac, 0x0);
	out_le32(0x30bb02b0, 0x240104c8);
	out_le32(0x30bb02b4, 0x0);
	out_le32(0x30bb02b8, 0x0);
	out_le32(0x30bb02bc, 0x0);
	out_le32(0x30bb02c0, 0x200104c5);
	out_le32(0x30bb02c4, 0x0);
	out_le32(0x30bb02c8, 0x0);
	out_le32(0x30bb02cc, 0x0);
	out_le32(0x30bb02d0, 0x465);
	out_le32(0x30bb02d4, 0x0);
	out_le32(0x30bb02d8, 0x0);
	out_le32(0x30bb02dc, 0x0);
	out_le32(0x30bb02e0, 0x461);
	out_le32(0x30bb02e4, 0x0);
	out_le32(0x30bb02e8, 0x0);
	out_le32(0x30bb02ec, 0x0);
	out_le32(0x30bb0300, 0x24010470);
	out_le32(0x30bb0304, 0x0);
	out_le32(0x30bb0308, 0x0);
	out_le32(0x30bb030c, 0x0);
	out_le32(0x30bb0310, 0x4b7);
	out_le32(0x30bb0314, 0x0);
	out_le32(0x30bb0318, 0x0);
	out_le32(0x30bb031c, 0x0);

	out_le32(0x30bb0018, 0x5af05af0); // lock LUT
	out_le32(0x30bb001c, 0x1);

	// init AHB read
	out_le32(0x30bb0020, 0x0);
	out_le32(0x30bb0024, 0x0);
	out_le32(0x30bb0028, 0x0);
	out_le32(0x30bb002c, 0x0);
	out_le32(0x30bb0030, 0x0);
	out_le32(0x30bb0034, 0x0);
	out_le32(0x30bb0038, 0x0);
	out_le32(0x30bb003c, 0x80000100);
	out_le32(0x30bb000c, 0x20);
	out_le32(0x30bb0080, 0x2);

	// temp = in_le32(0x30bb0000); // enable the module
	// out_le32(0x30bb0000, temp & (~0x2));
}

static void write_fspi_byte(unsigned int offset, unsigned char data)
{
	// invalidate AHB
	//
	unsigned int mcr0 = in_le32(0x30bb0000); // invoke SW reset
	mcr0 |= 0x1;
	out_le32(0x30bb0000, mcr0);


	while (in_le32(0x30bb0000) & 1)   // wait for it to complete
		;

	out_le32(0x30bb00bc, 0x1);  // clear TX FIFO
	out_le32(0x30bb00a0, 0x0);  // set AMBA base
	out_le32(0x30bb00a4, 0x10000);  // write enable command offset
	out_le32(0x30bb00b0, 0x1);  // trigger the command

	while (!(in_le32(0x30bb0014) & 0x1)) // wait for the command done
		;

	out_le32(0x30bb0014, 0x1);  // clear the status
	out_le32(0x30bb00bc, 0x1);  // clear TX FIFO
	out_le32(0x30bb00a0, offset);  // destination

	while (!(in_le32(0x30bb0014) & 0x40))  // wait until TX is empty
		;

	* (unsigned char *) 0x30bb0180 = data; // write the data to the TX FIFO

	out_le32(0x30bb0014, 0x40); // enable TX empty INT
	out_le32(0x30bb00a4, 0x60001);  // program page command offset
	out_le32(0x30bb00b0, 0x1);  // trigger the command

	while (!(in_le32(0x30bb0014) & 0x1)) // wait for the command done
		;

	out_le32(0x30bb00bc, 0x1); // clear TX FIFO
	out_le32(0x30bb0014, 0x1); // clear status, disable FIFO

	do
	{
		out_le32(0x30bb00b8, 0x1); // clear RX FIFO
		out_le32(0x30bb00a0, 0x0); // set AMBA base
		out_le32(0x30bb00a4, 0x30001); // 'read status register' command offset
		out_le32(0x30bb00b0, 0x1); // trigger the command

		while (!(in_le32(0x30bb0014) & 0x1)) // wait for the command done
			;

	} while ( in_le32(0x30bb0100) & 0x1 ); // until busy

	out_le32(0x30bb0014, 0x20);
	out_le32(0x30bb00b8, 0x1);
	out_le32(0x30bb0014, 0x1);
}

static unsigned int get_memory_configuration_byte(void)
{
	unsigned int flashByte0 = 0;
	unsigned int flashByte1 = 0;
	unsigned int flashByte2 = 0;
	unsigned int flashByte3 = 0;

	flashByte0 = * ( unsigned char * ) ( FLASH_AHB_START + FLASH_MEMORY_CONFIG_OFFSET + 0 );
	flashByte1 = * ( unsigned char * ) ( FLASH_AHB_START + FLASH_MEMORY_CONFIG_OFFSET + 1 );
	flashByte2 = * ( unsigned char * ) ( FLASH_AHB_START + FLASH_MEMORY_CONFIG_OFFSET + 2 );
	flashByte3 = * ( unsigned char * ) ( FLASH_AHB_START + FLASH_MEMORY_CONFIG_OFFSET + 3 );

	if ( ( flashByte0 == flashByte1 ) &&
	     ( flashByte1 == flashByte2 ) &&
	     ( flashByte2 == flashByte3 )
	   )
	{
		return flashByte0;
	}
	else
	{
		return 0;
	}
}

static void write_memory_configuration_byte(unsigned char flashByte)
{
	printf("Storing memory configuration to 0x%x\n", FLASH_MEMORY_CONFIG_OFFSET);

	write_fspi_byte( FLASH_MEMORY_CONFIG_OFFSET + 0, flashByte );
	write_fspi_byte( FLASH_MEMORY_CONFIG_OFFSET + 1, flashByte );
	write_fspi_byte( FLASH_MEMORY_CONFIG_OFFSET + 2, flashByte );
	write_fspi_byte( FLASH_MEMORY_CONFIG_OFFSET + 3, flashByte );
}

static void memory_detect(void)
{
	unsigned char memVarByte = get_memory_configuration_byte();
	mVariant_t mVarDetected = lpddr_invalid;

	if ( memVarByte == MEM_4G_VARIANT_ID )
	{
		printf("Memory configuration found (4G)\n");
		spl_dram_init(lpddr_4G);
	}
	else if ( memVarByte == MEM_2G_VARIANT_ID )
	{
		printf("Memory configuration found (2G)\n");
		spl_dram_init(lpddr_2G);
	}
	else if ( memVarByte == MEM_1G_VARIANT_ID )
	{
		printf("Memory configuration found (1G)\n");
		spl_dram_init(lpddr_1G);
	}
	else
	{
		printf("No valid memory configuration found ... trying autodetection\n");
		if ( spl_dram_init(lpddr_4G) )
		{ // 4G variant
			if ( get_memory_variant() == lpddr_4G )
			{
				printf("4G variant confirmed\n");
				mVarDetected = MEM_4G_VARIANT_ID;
			}
		}
		else if ( spl_dram_init(lpddr_2G) )
		{ // either 1G or 2G variant
			if ( get_memory_variant() == lpddr_2G )
			{
				printf("2G variant confirmed\n");
				mVarDetected = MEM_2G_VARIANT_ID;
			}
			else if ( spl_dram_init(lpddr_1G) && ( get_memory_variant() == lpddr_1G ) )
			{
				printf("1G variant confirmed\n");
				mVarDetected = MEM_1G_VARIANT_ID;
			}
		}

		// autodetection should be finished here, let's look at the results
		//
		if ( mVarDetected == lpddr_invalid )
		{
			printf("No memory configuration found and memory autodetection failed ... halting\n");
			while (1)
				;
		}

		if ( memVarByte == MEMORY_UNCONFIGURED )
		{ // blank configuration area => try to store the detected memory configuration there
			write_memory_configuration_byte( mVarDetected );
		}
		else
		{
			printf("Invalid memory configuration entry found - check flash memory at FLASH_MEMORY_CONFIG_OFFSET\n");
		}
	}
}

void board_init_f(ulong dummy)
{
	int ret;

	/* Clear global data */
	memset((void *)gd, 0, sizeof(gd_t));

	arch_cpu_init();

	board_early_init_f();

	timer_init();

	preloader_console_init();

	/* Clear the BSS. */
	memset(__bss_start, 0, __bss_end - __bss_start);

	ret = spl_init();
	if (ret) {
		debug("spl_init() failed: %d\n", ret);
		hang();
	}

	enable_tzc380();

	/* Adjust pmic voltage to 1.0V for 800M */
	setup_i2c(0, CONFIG_SYS_I2C_SPEED, 0x7f, &i2c_pad_info1);

	power_init_board();

	board_init();

	fspi_init();

	/* DDR initialization */
	memory_detect();

	poweron_and_tune_usb();

	board_init_r(NULL, 0);
}
