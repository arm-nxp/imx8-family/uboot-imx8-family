/*
 * Copyright 2018 NXP
 * Copyright 2020 congatec AG
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <malloc.h>
#include <errno.h>
#include <asm/io.h>
#include <miiphy.h>
#include <netdev.h>
#include <dm/uclass.h>
#include <asm/mach-imx/iomux-v3.h>
#include <asm-generic/gpio.h>
#include <fsl_esdhc.h>
#include <mmc.h>
#include <asm/arch/imx8mm_pins.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/gpio.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <asm/arch/clock.h>
#include <spl.h>
#include <asm/mach-imx/dma.h>
#include <power/pmic.h>
#include <power/bd71837.h>
#include <usb.h>
#include <sec_mipi_dsim.h>
#include <imx_mipi_dsi_bridge.h>
#include <mipi_dsi_panel.h>
#include <asm/mach-imx/video.h>
#include <asm/arch/imx8m_ddr.h>
#include "video_params_cgtsx8m.h"

DECLARE_GLOBAL_DATA_PTR;

struct pms {
  unsigned int p;
  unsigned int m;
  unsigned int s;
  unsigned int pms;
  unsigned int dsifreq;
  unsigned int sndiv;
  unsigned int difference;
  uint64_t bit_freq;
  uint64_t pix_freq;
};

#define	PS2KHZ(ps)	(1000000000UL / (ps))

#define UART_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_FSEL1)
#define WDOG_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_ODE | PAD_CTL_PUE | PAD_CTL_PE)

/* PADs for all 4 SX8M UARTs are configured here (RX/TX only) */
static iomux_v3_cfg_t const uart_pads[] = {
	IMX8MM_PAD_UART1_RXD_UART1_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_UART1_TXD_UART1_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_UART2_RXD_UART2_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_UART2_TXD_UART2_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_SCLK_UART3_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_MOSI_UART3_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_UART4_RXD_UART4_RX | MUX_PAD_CTRL(UART_PAD_CTRL),
	IMX8MM_PAD_UART4_TXD_UART4_TX | MUX_PAD_CTRL(UART_PAD_CTRL),
};

static iomux_v3_cfg_t const wdog_pads[] = {
	IMX8MM_PAD_GPIO1_IO02_WDOG1_WDOG_B  | MUX_PAD_CTRL(WDOG_PAD_CTRL),
};

#ifdef CONFIG_FSL_FSPI
#define QSPI_PAD_CTRL	(PAD_CTL_DSE2 | PAD_CTL_HYS)
static iomux_v3_cfg_t const qspi_pads[] = {
	IMX8MM_PAD_NAND_ALE_QSPI_A_SCLK | MUX_PAD_CTRL(QSPI_PAD_CTRL | PAD_CTL_PE | PAD_CTL_PUE),
	IMX8MM_PAD_NAND_CE0_B_QSPI_A_SS0_B | MUX_PAD_CTRL(QSPI_PAD_CTRL),

	IMX8MM_PAD_NAND_DATA00_QSPI_A_DATA0 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA01_QSPI_A_DATA1 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA02_QSPI_A_DATA2 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA03_QSPI_A_DATA3 | MUX_PAD_CTRL(QSPI_PAD_CTRL),
};

int board_qspi_init(void)
{
	imx_iomux_v3_setup_multiple_pads(qspi_pads, ARRAY_SIZE(qspi_pads));

	set_clk_qspi();

	return 0;
}
#endif

#ifdef CONFIG_MXC_SPI
#define SPI_PAD_CTRL	(PAD_CTL_DSE2 | PAD_CTL_HYS)
static iomux_v3_cfg_t const ecspi1_pads[] = {
	IMX8MM_PAD_ECSPI1_SCLK_ECSPI1_SCLK | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_MOSI_ECSPI1_MOSI | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_MISO_ECSPI1_MISO | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI1_SS0_GPIO5_IO9 | MUX_PAD_CTRL(NO_PAD_CTRL),
};

static iomux_v3_cfg_t const ecspi2_pads[] = {
	IMX8MM_PAD_ECSPI2_SCLK_ECSPI2_SCLK | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI2_MOSI_ECSPI2_MOSI | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI2_MISO_ECSPI2_MISO | MUX_PAD_CTRL(SPI_PAD_CTRL),
	IMX8MM_PAD_ECSPI2_SS0_GPIO5_IO13 | MUX_PAD_CTRL(NO_PAD_CTRL),
};

static void setup_spi(void)
{
	imx_iomux_v3_setup_multiple_pads(ecspi1_pads, ARRAY_SIZE(ecspi1_pads));
	imx_iomux_v3_setup_multiple_pads(ecspi2_pads, ARRAY_SIZE(ecspi2_pads));
	gpio_request(IMX_GPIO_NR(5, 9), "ECSPI1 CS");
	gpio_request(IMX_GPIO_NR(5, 13), "ECSPI2 CS");
}

int board_spi_cs_gpio(unsigned bus, unsigned cs)
{
	if (bus == 0)
		return IMX_GPIO_NR(5, 9);
	else
		return IMX_GPIO_NR(5, 13);
}
#endif

#ifdef CONFIG_NAND_MXS
#define NAND_PAD_CTRL	(PAD_CTL_DSE6 | PAD_CTL_FSEL2 | PAD_CTL_HYS)
#define NAND_PAD_READY0_CTRL (PAD_CTL_DSE6 | PAD_CTL_FSEL2 | PAD_CTL_PUE)
static iomux_v3_cfg_t const gpmi_pads[] = {
	IMX8MM_PAD_NAND_ALE_RAWNAND_ALE | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_CE0_B_RAWNAND_CE0_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_CE1_B_RAWNAND_CE1_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_CLE_RAWNAND_CLE | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA00_RAWNAND_DATA00 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA01_RAWNAND_DATA01 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA02_RAWNAND_DATA02 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA03_RAWNAND_DATA03 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA04_RAWNAND_DATA04 | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA05_RAWNAND_DATA05	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA06_RAWNAND_DATA06	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_DATA07_RAWNAND_DATA07	| MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_RE_B_RAWNAND_RE_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_READY_B_RAWNAND_READY_B | MUX_PAD_CTRL(NAND_PAD_READY0_CTRL),
	IMX8MM_PAD_NAND_WE_B_RAWNAND_WE_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
	IMX8MM_PAD_NAND_WP_B_RAWNAND_WP_B | MUX_PAD_CTRL(NAND_PAD_CTRL),
};

static void setup_gpmi_nand(void)
{
	imx_iomux_v3_setup_multiple_pads(gpmi_pads, ARRAY_SIZE(gpmi_pads));
}
#endif

int board_early_init_f(void)
{
	struct wdog_regs *wdog = (struct wdog_regs *)WDOG1_BASE_ADDR;

	imx_iomux_v3_setup_multiple_pads(wdog_pads, ARRAY_SIZE(wdog_pads));

	set_wdog_reset(wdog);

	imx_iomux_v3_setup_multiple_pads(uart_pads, ARRAY_SIZE(uart_pads));

#ifdef CONFIG_NAND_MXS
	setup_gpmi_nand(); /* SPL will call the board_early_init_f */
#endif

	return 0;
}

#ifdef CONFIG_BOARD_POSTCLK_INIT
int board_postclk_init(void)
{
	/* TODO */
	return 0;
}
#endif

/* Tries to identify the current memory configuration by checking the pattern
 * produced by memory aliasing
 * MMU has to be off when this function is called
 */
mVariant_t get_memory_variant(void)
{
	unsigned int storage[4];
	mVariant_t mVarIdentified = lpddr_invalid;

	// store the data at testing locations
	storage[0] = in_le32(ADDR_START_1G);
	storage[1] = in_le32(ADDR_START_2G);
	storage[2] = in_le32(ADDR_START_3G);
	storage[3] = in_le32(ADDR_START_4G);

	// write testing patterns
	out_le32( ADDR_START_1G, 0x11111111 );
	out_le32( ADDR_START_2G, 0x22222222 );
	out_le32( ADDR_START_3G, 0x33333333 );
	out_le32( ADDR_START_4G, 0x44444444 );

	if ( ( * ( ( unsigned int * ) ADDR_START_1G ) == 0x11111111 ) &&
	     ( * ( ( unsigned int * ) ADDR_START_2G ) == 0x22222222 ) &&
	     ( * ( ( unsigned int * ) ADDR_START_3G ) == 0x33333333 ) &&
	     ( * ( ( unsigned int * ) ADDR_START_4G ) == 0x44444444 ) &&
	     ( * ( ( unsigned int * ) ADDR_START_5G ) == 0x11111111 )
	   )
	{ // 4G variant identified
		// restore data
		out_le32( ADDR_START_1G, storage[0] );
		out_le32( ADDR_START_2G, storage[1] );
		out_le32( ADDR_START_3G, storage[2] );
		out_le32( ADDR_START_4G, storage[3] );

		mVarIdentified = lpddr_4G;
	}
	else if ( ( * ( ( unsigned int * ) ADDR_START_1G ) == 0x33333333 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_2G ) == 0x44444444 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_3G ) == 0x33333333 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_4G ) == 0x44444444 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_5G ) == 0x33333333 )
	        )
	{ // 2G variant identified
		// restore data
		out_le32( ADDR_START_1G, storage[0] );
		out_le32( ADDR_START_2G, storage[1] );

		mVarIdentified = lpddr_2G;
	}
	else if ( ( * ( ( unsigned int * ) ADDR_START_1G ) == 0x44444444 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_2G ) == 0x44444444 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_3G ) == 0x44444444 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_4G ) == 0x44444444 ) &&
	          ( * ( ( unsigned int * ) ADDR_START_5G ) == 0x44444444 )
		)
	{ // 1G variant identified
		// restore data
		out_le32( ADDR_START_1G, storage[0] );

		mVarIdentified = lpddr_1G;
	}

	return mVarIdentified;
}

// works before MMU has been turned on
// detects and returns memory size
//
phys_size_t detect_phys_ram_size(void)
{
	switch ( get_memory_variant() )
	{
		case lpddr_4G:
		  return 0x100000000;
		 break;
		case lpddr_2G:
		  return 0x80000000;
		 break;
		case lpddr_1G:
		  return 0x40000000;
		 break;
		default:
		  return 0;
		 break;
	}
}

// returns memory size stored by dram_init into the gd->ram_size
// this function shall be used after MMU has been turned on
//
phys_size_t get_phys_ram_size(void)
{
	return gd->ram_size;
}

int dram_init(void)
{
	/* rom_pointer[1] contains the size of TEE occupies */
	if (rom_pointer[1])
		gd->ram_size = detect_phys_ram_size() - rom_pointer[1];
	else
		gd->ram_size = detect_phys_ram_size();

	return 0;
}

#ifdef CONFIG_OF_BOARD_SETUP
int ft_board_setup(void *blob, bd_t *bd)
{
	return 0;
}
#endif

#ifdef CONFIG_FEC_MXC
#define FEC_RST_PAD IMX_GPIO_NR(4, 22)
static iomux_v3_cfg_t const fec1_rst_pads[] = {
	IMX8MM_PAD_SAI2_RXC_GPIO4_IO22 | MUX_PAD_CTRL(NO_PAD_CTRL),
};

static void setup_iomux_fec(void)
{
	imx_iomux_v3_setup_multiple_pads(fec1_rst_pads,
					 ARRAY_SIZE(fec1_rst_pads));

	gpio_request(FEC_RST_PAD, "fec1_rst");
	gpio_direction_output(FEC_RST_PAD, 0);
	udelay(500);
	gpio_direction_output(FEC_RST_PAD, 1);
}

static int setup_fec(void)
{
	struct iomuxc_gpr_base_regs *const iomuxc_gpr_regs
		= (struct iomuxc_gpr_base_regs *) IOMUXC_GPR_BASE_ADDR;

	setup_iomux_fec();

	/* Use 125M anatop REF_CLK1 for ENET1, not from external */
	clrsetbits_le32(&iomuxc_gpr_regs->gpr[1],
			IOMUXC_GPR_GPR1_GPR_ENET1_TX_CLK_SEL_SHIFT, 0);
	return set_clk_enet(ENET_125MHZ);
}

int board_phy_config(struct phy_device *phydev)
{
	/* enable rgmii rxc skew and phy mode select to RGMII copper */
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x1f);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, 0x8);

	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x00);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, 0x82ee);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x05);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, 0x100);

	if (phydev->drv->config)
		phydev->drv->config(phydev);
	return 0;
}
#endif

int board_init(void)
{
#ifdef CONFIG_MXC_SPI
	setup_spi();
#endif

#ifdef CONFIG_FEC_MXC
	setup_fec();
#endif

#ifdef CONFIG_FSL_FSPI
	board_qspi_init();
#endif

	return 0;
}

int board_mmc_get_env_dev(int devno)
{
	return devno - 1;
}

int mmc_map_to_kernel_blk(int devno)
{
	return devno + 1;
}

#ifdef CONFIG_VIDEO_MXS

#define SN65DSI84 0x2c
#define SN65DSI86 0x2d

#define DSI86_PLL_LOCKED_MASK 0x80
#define DSI86_TX_NORMAL_MODE  0x1

/* fixed phy ref clk rate */
#define PHY_REF_CLK             27000000
#define VIDEO_PLL_RATE          594000000UL

#define MIPI_BPP                24
#define MIPI_LANES              4

#define LVDS18h_CHB_24BPP_FORMAT1_OFFSET	0
#define LVDS18h_CHA_24BPP_FORMAT1_OFFSET	1
#define LVDS18h_CHB_24BPP_MODE_OFFSET		2
#define LVDS18h_CHA_24BPP_MODE_OFFSET		3
#define LVDS18h_LVDS_LINK_CFG_OFFSET		4
#define LVDS18h_VS_NEG_POLARITY_OFFSET		5
#define LVDS18h_HS_NEG_POLARITY_OFFSET		6
#define LVDS18h_DE_NEG_POLARITY_OFFSET		7

static const struct sec_mipi_dsim_plat_data imx8mm_mipi_dsim_plat_data = {
	.version	= 0x1060200,
	.max_data_lanes = 4,
	.max_data_rate  = 1500000000ULL,
	.reg_base = MIPI_DSI_BASE_ADDR,
	.gpr_base = CSI_BASE_ADDR + 0x8000,
};

static int sn65dsi_write(struct udevice *dev, uint addr, uint mask, uint data)
{
	uint8_t valb;
	int err;

	if (mask != 0xff) {
		err = dm_i2c_read(dev, addr, &valb, 1);
		if (err)
			return err;

		valb &= ~mask;
		valb |= data;
	} else {
		valb = data;
	}

	err = dm_i2c_write(dev, addr, &valb, 1);

	mdelay(15);
	return err;
}

static int sn65dsi_read(struct udevice *dev, uint8_t addr, uint8_t *data)
{
	uint8_t valb;
	int err;

	err = dm_i2c_read(dev, addr, &valb, 1);
	if (err)
		return err;

	*data = valb;
	return 0;
}

static struct pms mipi_freq_calc( uint64_t inFreq, unsigned int lcddiv)
{
        struct pms combFound = { 0, 0, 0, 0, 0, 0 };
        struct pms zeroBackup = { 0, 0, 0, 0, 0, 0 };
        uint64_t pmstability, pstability;
        uint64_t freq, bit_freq, pix_freq;

        pix_freq = VIDEO_PLL_RATE / lcddiv; // LCDIF frequency

        bit_freq = ( VIDEO_PLL_RATE * MIPI_BPP ) / ( MIPI_LANES * lcddiv ); // minimal DSI frequency
        bit_freq >>= 1; // DSI is DDR

        debug("Calc bitfreq is: %llu\n", bit_freq);
        debug("Calc pixfreq is: %llu\n", pix_freq);

        for (unsigned int s=0;s<=1;s++)
          for (unsigned int p=1;p<33;p++)
            for (unsigned int m=25;m<=125;m++)
            {
                   pmstability = ( inFreq * m ) / p;
                   pstability = inFreq / p;
                   freq = ( inFreq * m ) / ( p * ( 1 << s ) );

                   debug( "Frequency candidate: %llu - PMS: %u %u %u\n", freq, p, m, s );
                   if ( ( pmstability >= 350000000 ) && ( pmstability <= 750000000 ) &&
                        ( pstability >= 6000000 ) && ( pstability <= 12000000 ) &&
                        ( ( freq >= ( bit_freq ) ) && ( freq <= 600000000 ) )
                      )
                   {
                     unsigned int sndiv; // encapsulating the requirement 2^x into the cycle
                     for (sndiv=16; ( freq / sndiv ) < pix_freq; sndiv>>=1)
                       ;

                     int current_difference = ( freq / sndiv ) - pix_freq;

                     if ( current_difference == 0 )
                     {
                       zeroBackup.p = p;
                       zeroBackup.m = m;
                       zeroBackup.s = s;
                       zeroBackup.pms = (zeroBackup.s) | (zeroBackup.m << 4) | (zeroBackup.p << 13);
                       zeroBackup.dsifreq = freq;
                       zeroBackup.sndiv = sndiv;
                       zeroBackup.difference = current_difference;
                     }
 
                     if ( ( ( current_difference > 0 ) && ( current_difference < 5000000 ) ) &&
                          ( ( combFound.p == 0 )  ||
                            ( ( ( combFound.difference > current_difference ) &&
                                ( sndiv >= combFound.sndiv )
                              ) 
                            )
                          )
                        )
                     {

                       combFound.p = p;
                       combFound.m = m;
                       combFound.s = s;
                       combFound.pms = (combFound.s) | (combFound.m << 4) | (combFound.p << 13);
                       combFound.dsifreq = freq;
                       combFound.sndiv = sndiv;
                       combFound.difference = current_difference;

                       debug("Difference is: %u\n", combFound.difference);
                     }
                   }
             }

        if ( combFound.p == 0 )
          combFound = zeroBackup;

        combFound.pix_freq = pix_freq;
        combFound.bit_freq = bit_freq;

        debug( "Frequency chosen: %u - PMS: %u %u %u\n", combFound.dsifreq, combFound.p, combFound.m, combFound.s );

        return combFound;
}

static void sn65dsi84_init( struct display_info_t const *dev,
                            struct udevice *sndev,
                            struct sec_mipi_dsim *dsim_host
			  )
{
	uint32_t div;
	struct pms combFound;

	div = VIDEO_PLL_RATE / 1000;
	div = (div + PS2KHZ(dev->mode.pixclock) - 1) / PS2KHZ(dev->mode.pixclock);

	if (div < 1)
	div = 1;

	combFound = mipi_freq_calc( PHY_REF_CLK,
	                            div
	                          ); 

	mdelay(20);

	dsim_host->p = combFound.p;
	dsim_host->m = combFound.m;
	dsim_host->s = combFound.s;
	dsim_host->pms = combFound.pms;
	dsim_host->pix_clk = combFound.pix_freq;
	dsim_host->bit_clk = combFound.bit_freq;

	if ( combFound.m == 0 )
	{
		printf("DSI PHY frequency error - no suitable found\n");
		return; // dsim_host attributes are filled by zeros in the previous step
	}

	if ( dev->mode.vmode & FB_VMODE_LVDS_IS_TWO_CHANNEL_MASK )
	{
		combFound.sndiv<<=1;
	}

	// DSI clock divider setting
	sn65dsi_write(sndev, 0x0B, 0xff, (combFound.sndiv - 1) << 3);

	// calculation of LVDS clock range
	unsigned int lvdsclock = combFound.dsifreq / combFound.sndiv;
	unsigned char lvds_clk_range;

	if ( lvdsclock < 37500000 )
		lvds_clk_range = 0x0;
	else if ( lvdsclock < 62500000 )
		lvds_clk_range = 0x1;
	else if ( lvdsclock < 87500000 )
		lvds_clk_range = 0x2;
	else if ( lvdsclock < 112500000 )
		lvds_clk_range = 0x3;
	else if ( lvdsclock < 137500000 )
		lvds_clk_range = 0x4;
	else
		lvds_clk_range = 0x5;

	sn65dsi_write(sndev, 0x0A, 0x0f, ( lvds_clk_range << 1) | 0x1 );

	// MIPI DSI parameters setting
	sn65dsi_write(sndev, 0x10, 0x18, 0x0); // 4 DSI lanes
	sn65dsi_write(sndev, 0x12, 0xff, combFound.dsifreq / 5000000); // DSI freq range

	// LVDS configuration setting
	sn65dsi_write(sndev, 0x18, 0xff,
			( ( dev->mode.sync & FB_SYNC_HSYNC_ACTIVE_LOW_MASK ? 1 << LVDS18h_HS_NEG_POLARITY_OFFSET : 0 ) |
			  ( dev->mode.sync & FB_SYNC_VSYNC_ACTIVE_LOW_MASK ? 1 << LVDS18h_VS_NEG_POLARITY_OFFSET : 0 ) |
			  ( dev->mode.sync & FB_SYNC_DE_ACTIVE_LOW_MASK ? 1 << LVDS18h_DE_NEG_POLARITY_OFFSET : 0 ) |
			  ( dev->mode.vmode & FB_VMODE_LVDS_BRIDGE_BPPF_IS_JEIDA_MASK ? 1 << LVDS18h_CHA_24BPP_FORMAT1_OFFSET : 0 ) |
			  ( dev->mode.vmode & FB_VMODE_LVDS_BRIDGE_BPPF_IS_JEIDA_MASK ? 1 << LVDS18h_CHB_24BPP_FORMAT1_OFFSET : 0 ) |
			  ( dev->mode.vmode & FB_VMODE_BRIDGE_BPP_IS_18_MASK ? 0 : 1 << LVDS18h_CHA_24BPP_MODE_OFFSET ) |
			  ( dev->mode.vmode & FB_VMODE_BRIDGE_BPP_IS_18_MASK ? 0 : 1 << LVDS18h_CHB_24BPP_MODE_OFFSET ) |
			  ( dev->mode.vmode & FB_VMODE_LVDS_IS_TWO_CHANNEL_MASK ? 0 : 1 << LVDS18h_LVDS_LINK_CFG_OFFSET )
			)
		     );

	// display parameters setting
	sn65dsi_write(sndev, 0x20, 0xff, dev->mode.xres & 0xff); // Line Len - lower 8 bits of the 12
	sn65dsi_write(sndev, 0x21, 0xff, ( dev->mode.xres >> 8 ) & 0xff); // Line Len -   high 4 bits of the 12

	sn65dsi_write(sndev, 0x28, 0xff, 0x20); // sync delay - lower 8 bits of the 12
	sn65dsi_write(sndev, 0x29, 0xff, 0x0); // sync delay -  high 4 bits of the 12

	unsigned int val32;

	val32 = dev->mode.hsync_len;

	if ( dev->mode.vmode & FB_VMODE_LVDS_IS_TWO_CHANNEL_MASK )
	{
		val32 = val32 >> 1;
	}

	sn65dsi_write(sndev, 0x2C, 0xff, val32 & 0xff); // hsync pulse width - lower 8 bits of the 12
	sn65dsi_write(sndev, 0x2D, 0xff, ( val32 >> 8 ) & 0xff ); // hsync pulse width -  high 4 bits of the 12

	sn65dsi_write(sndev, 0x30, 0xff, dev->mode.vsync_len & 0xff); // vsync pulse width - lower 8 bits of the 12
	sn65dsi_write(sndev, 0x31, 0xff, ( dev->mode.vsync_len >> 8 ) & 0xff); // vsync pulse width -  high 4 bits of the 12

	val32 = dev->mode.left_margin;

	if ( dev->mode.vmode & FB_VMODE_LVDS_IS_TWO_CHANNEL_MASK )
	{
		val32 = val32 >> 1;
	}

	sn65dsi_write(sndev, 0x34, 0xff, val32 & 0xff); // horizontal back porch

	// PLL enable
	sn65dsi_write(sndev, 0x0D, 0xff, 0x1);

	mdelay(15);

	// 7. Set the soft reset bit (SCR 0x09.0)
	sn65dsi_write(sndev, 0x09, 0xff, 0x1);

	mdelay(15);
}

static void sn65dsi86_init( struct display_info_t const *dev,
                            struct udevice *sndev,
                            struct sec_mipi_dsim *dsim_host
                          )
{
	uint32_t div;
	struct pms combFound;

	div = VIDEO_PLL_RATE / 1000;
	div = (div + PS2KHZ(dev->mode.pixclock) - 1) / PS2KHZ(dev->mode.pixclock);

	if (div < 1)
	div = 1;

	combFound = mipi_freq_calc( PHY_REF_CLK,
	                            div
				  );

	mdelay(20);

	dsim_host->p = combFound.p;
	dsim_host->m = combFound.m;
	dsim_host->s = combFound.s;
	dsim_host->pms = combFound.pms;
	dsim_host->pix_clk = combFound.pix_freq;
	dsim_host->bit_clk = combFound.bit_freq;

	if ( combFound.m == 0 )
	{
		printf("DSI PHY frequency error - no suitable found\n");
		return; // dsim_host attributes are filled by zeros in the previous step
	}

	// 5. Initialize CSR registers
	mdelay(20);

	sn65dsi_write(sndev, 0x10, 0xff, 0x26); // 4 DSI lanes
	sn65dsi_write(sndev, 0x12, 0xff, combFound.dsifreq / 5000000); // DSIA freq range

	if (((dev->mode.xres & 0xffff) * (dev->mode.yres & 0xffff)) < 1350000 )
	{
		printf("Using 1 HBR lane\n");
		sn65dsi_write(sndev, 0x93, 0xff, 0x10); // 1 DP lane
	}
	else
	{
		printf("Using 2 HBR lanes\n");
		sn65dsi_write(sndev, 0x93, 0xff, 0x20); // 2 DP lanes
	}

	sn65dsi_write(sndev, 0x94, 0xff, 0x80); // HBR (2.7Gbps)

	sn65dsi_write(sndev, 0xd,  0xff, 0x1 ); // enable PLL

	unsigned int timeout = 10000;
	uint8_t reg_0A_val;

	do {
		sn65dsi_read(sndev, 0x0A, &reg_0A_val);
	} while ( ( timeout-- > 0 ) &&
		  ! ( reg_0A_val & DSI86_PLL_LOCKED_MASK )
		);

	if ( ! ( reg_0A_val & DSI86_PLL_LOCKED_MASK ) )
	{
		printf("Error locking PLL\n");
		return;
	}

	sn65dsi_write(sndev, 0xff, 0x7, 0x7 ); // select Page 7
	sn65dsi_write(sndev, 0x16, 0x1, 0x1 ); // make ASSR control RW
	sn65dsi_write(sndev, 0xff, 0x7, 0x0 ); // select Page 0

	sn65dsi_write(sndev, 0x5a, 0x3, 0x0 ); // switch to DP scrambler seed

	sn65dsi_write(sndev, 0x95, 0xff, 0x0 ); // POST-cursor 2 0dB

	mdelay(10);

	sn65dsi_write(sndev, 0x96, 0xff, 0xa); // semi-auto train

	mdelay(20);

	timeout = 10000;
	uint8_t reg_96_val;

	do {
		sn65dsi_read(sndev, 0x96, &reg_96_val);
	} while ( ( timeout-- > 0 ) &&
		  ( reg_96_val != DSI86_TX_NORMAL_MODE )
		);

	sn65dsi_read(sndev, 0x96, &reg_96_val);

	if ( reg_96_val != DSI86_TX_NORMAL_MODE )
	{
		printf("Error training line\n");
		return;
	}

	sn65dsi_write(sndev, 0x20, 0xff, dev->mode.xres & 0xff);
	sn65dsi_write(sndev, 0x21, 0xff, ( dev->mode.xres >> 8 ) & 0xff);

	sn65dsi_write(sndev, 0x24, 0xff, dev->mode.yres & 0xff);
	sn65dsi_write(sndev, 0x25, 0xff, ( dev->mode.yres >> 8 ) & 0xff);

	sn65dsi_write	(sndev, 0x2c, 0xff, dev->mode.hsync_len & 0xff);
	sn65dsi_write	(sndev, 0x2d, 0xff, ( ( dev->mode.hsync_len >> 8 ) & 0x7f ) |
					    ( ( FB_SYNC_HSYNC_ACTIVE_LOW_MASK & dev->mode.sync )
					      ? 1 << 7
					      : 0
					    )
			);

	sn65dsi_write	(sndev, 0x30, 0xff, dev->mode.vsync_len & 0xff);
	sn65dsi_write	(sndev, 0x31, 0xff, ( ( dev->mode.vsync_len >> 8 ) & 0x7f ) |
					    ( ( FB_SYNC_VSYNC_ACTIVE_LOW_MASK & dev->mode.sync )
					      ? 1 << 7
					      : 0
					    )
			);

	sn65dsi_write(sndev, 0x34, 0xff, dev->mode.left_margin & 0xff); // HBP
	sn65dsi_write(sndev, 0x36, 0xff, dev->mode.upper_margin & 0xff); // VBP
	sn65dsi_write(sndev, 0x38, 0xff, dev->mode.right_margin & 0xff); // HFP
	sn65dsi_write(sndev, 0x3a, 0xff, dev->mode.lower_margin & 0xff); // VFP

	if ( dev->mode.vmode & FB_VMODE_BRIDGE_BPP_IS_18_MASK )
	{
	  sn65dsi_write(sndev, 0x5b, 0xff, 0x1); // 18 BPP
	}
	else
	{
	  sn65dsi_write(sndev, 0x5b, 0xff, 0x0); // 24 BPP
	}

	sn65dsi_write(sndev, 0x3c, 0xff, 0x0); // disable color bar

	sn65dsi_write(sndev, 0x5a, 0xff, 0xc); // enh.framing, DP standard, vstream enable
}

#define DISPLAY_MIX_SFT_RSTN_CSR		0x00
#define DISPLAY_MIX_CLK_EN_CSR		0x04

   /* 'DISP_MIX_SFT_RSTN_CSR' bit fields */
#define BUS_RSTN_BLK_SYNC_SFT_EN	BIT(6)

   /* 'DISP_MIX_CLK_EN_CSR' bit fields */
#define LCDIF_PIXEL_CLK_SFT_EN		BIT(7)
#define LCDIF_APB_CLK_SFT_EN		BIT(6)

void disp_mix_bus_rstn_reset(ulong gpr_base, bool reset)
{
	if (!reset)
		/* release reset */
		setbits_le32(gpr_base + DISPLAY_MIX_SFT_RSTN_CSR, BUS_RSTN_BLK_SYNC_SFT_EN);
	else
		/* hold reset */
		clrbits_le32(gpr_base + DISPLAY_MIX_SFT_RSTN_CSR, BUS_RSTN_BLK_SYNC_SFT_EN);
}

void disp_mix_lcdif_clks_enable(ulong gpr_base, bool enable)
{
	if (enable)
		/* enable lcdif clks */
		setbits_le32(gpr_base + DISPLAY_MIX_CLK_EN_CSR, LCDIF_PIXEL_CLK_SFT_EN | LCDIF_APB_CLK_SFT_EN);
	else
		/* disable lcdif clks */
		clrbits_le32(gpr_base + DISPLAY_MIX_CLK_EN_CSR, LCDIF_PIXEL_CLK_SFT_EN | LCDIF_APB_CLK_SFT_EN);
}

struct mipi_dsi_client_dev sn65dsi84_dev = {
	.channel	= 0,
	.lanes = 4,
	.format  = MIPI_DSI_FMT_RGB888,
	.mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_SYNC_PULSE | MIPI_DSI_MODE_VIDEO_HFP |
			  MIPI_DSI_MODE_EOT_PACKET | MIPI_DSI_MODE_VIDEO_HSE | MIPI_DSI_MODE_VIDEO_HBP,
	.name = "SN65DSI84",
};

struct mipi_dsi_client_dev sn65dsi86_dev = {
	.channel	= 0,
	.lanes = 4,
	.format  = MIPI_DSI_FMT_RGB888,
	.mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_SYNC_PULSE | MIPI_DSI_MODE_VIDEO_HFP |
			  MIPI_DSI_MODE_EOT_PACKET | MIPI_DSI_MODE_VIDEO_HSE | MIPI_DSI_MODE_VIDEO_AUTO_VERT ,
	.name = "SN65DSI86",
};

struct mipi_dsi_client_dev mipidsi_panel_dev = {
	.channel	= 0,
	.lanes = 4,
	.format  = MIPI_DSI_FMT_RGB888,
	.mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_SYNC_PULSE | MIPI_DSI_MODE_VIDEO_HFP |
			  MIPI_DSI_MODE_EOT_PACKET | MIPI_DSI_MODE_VIDEO_HSE | MIPI_DSI_MODE_VIDEO_AUTO_VERT ,
	.name = "DSIPANEL",
};

#define FSL_SIP_GPC			0xC2000000
#define FSL_SIP_CONFIG_GPC_PM_DOMAIN	0x3
#define DISPMIX				9
#define MIPI				10

void do_enable_mipi2lvds(struct display_info_t const *dev, struct udevice *sndev)
{
	/* enable the dispmix & mipi phy power domain */
	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, DISPMIX, true, 0);
	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, MIPI, true, 0);

	/* Put lcdif out of reset */
	disp_mix_bus_rstn_reset(imx8mm_mipi_dsim_plat_data.gpr_base, false);
	disp_mix_lcdif_clks_enable(imx8mm_mipi_dsim_plat_data.gpr_base, true);

	/* Setup mipi dsim */
	struct sec_mipi_dsim *dsim_host = sec_mipi_dsim_setup(&imx8mm_mipi_dsim_plat_data);

	/* SN65DSI84 initialization */
	if ( dsim_host != NULL )
		sn65dsi84_init(dev, sndev, dsim_host);

	imx_mipi_dsi_bridge_attach(&sn65dsi84_dev); /* attach sn65dsi84 device */

	// set backlight on
	gpio_request(IMX_GPIO_NR(4, 19), "BKL EN");
	gpio_direction_output(IMX_GPIO_NR(4, 19), 1);

	// set BKL PWM to 100%
	gpio_request(IMX_GPIO_NR(1, 1), "DSI BL PWM");
	gpio_direction_output(IMX_GPIO_NR(1, 1), 1);
}

void do_enable_mipi2dp(struct display_info_t const *dev, struct udevice *sndev)
{
	/* enable the dispmix & mipi phy power domain */
	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, DISPMIX, true, 0);
	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, MIPI, true, 0);

	/* Put lcdif out of reset */
	disp_mix_bus_rstn_reset(imx8mm_mipi_dsim_plat_data.gpr_base, false);
	disp_mix_lcdif_clks_enable(imx8mm_mipi_dsim_plat_data.gpr_base, true);

	/* Setup mipi dsim */
	struct sec_mipi_dsim *dsim_host = sec_mipi_dsim_setup(&imx8mm_mipi_dsim_plat_data);

	/* SN65DSI84 initialization */
	if ( dsim_host != NULL )
		sn65dsi86_init(dev, sndev, dsim_host);

	imx_mipi_dsi_bridge_attach(&sn65dsi86_dev); /* attach sn65dsi86 device */
}

void do_enable_mipi_dsi_panel(struct display_info_t const *dev)
{
	printf("No bridge found - MIPI DSI panel needs to be configured\n");
#if 0
	/* enable the dispmix & mipi phy power domain */
	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, DISPMIX, true, 0);
	call_imx_sip(FSL_SIP_GPC, FSL_SIP_CONFIG_GPC_PM_DOMAIN, MIPI, true, 0);

	/* Put lcdif out of reset */
	disp_mix_bus_rstn_reset(imx8mm_mipi_dsim_plat_data.gpr_base, false);
	disp_mix_lcdif_clks_enable(imx8mm_mipi_dsim_plat_data.gpr_base, true);

	/* Setup mipi dsim */
	sec_mipi_dsim_setup(&imx8mm_mipi_dsim_plat_data);

	imx_mipi_dsi_bridge_attach(&mipidsi_panel_dev);
#endif
}

void do_enable_mipi_and_bridge(struct display_info_t const *dev)
{
	struct udevice *bus, *sndev;
	int i2c_bus = 3;
	uint8_t idString[6];

	unsigned int cma_placement_start = 1024;
	unsigned int cma_placement_end = 0;
	unsigned int cma_size = 0;

	char cmaString[13]; // size is based on the XXXM@Y-ZZZZM pattern


	gpio_request(IMX_GPIO_NR(1, 8), "DSI EN");
	gpio_direction_output(IMX_GPIO_NR(1, 8), 1);

	mdelay(20);

	if ( env_get("cma_settings") == NULL )
	{
		if ( get_phys_ram_size() <= 0x40000000 )
		{
			cma_size = 320;
			cma_placement_end = cma_placement_start + 512;
		}
		else if ( get_phys_ram_size() <= 0xA8000000 )
		{
			cma_size = 480;
			cma_placement_end = cma_placement_start + 1536;
		}
		else // we have more than 640M + 2048M
		{
			cma_size = 640;
			cma_placement_end = cma_placement_start + 2048;
		}

		sprintf(cmaString, "%dM@%dM-%dM", cma_size, cma_placement_start, cma_placement_end );
		env_set("cma_settings", cmaString);
	}

	if (uclass_get_device_by_seq(UCLASS_I2C, i2c_bus, &bus) == 0)
	{
		if (dm_i2c_probe(bus, SN65DSI84, 0, &sndev) == 0)
		{
			idString[5] = '\0';
			sn65dsi_read(sndev, 0x04, &idString[0]);
			sn65dsi_read(sndev, 0x03, &idString[1]);
			sn65dsi_read(sndev, 0x02, &idString[2]);
			sn65dsi_read(sndev, 0x01, &idString[3]);
			sn65dsi_read(sndev, 0x00, &idString[4]);

			if (strcmp((char *)idString, "DSI85") == 0)
			{
				printf("Attaching LVDS bridge %s\n", idString);
				do_enable_mipi2lvds(dev, sndev);

				env_set("video_output_type", "LVDS");
				return;
			}
			else
			{
				printf("LVDS bridge read error - %s\n", idString);
			}
		}
		else if (dm_i2c_probe(bus, SN65DSI86, 0, &sndev) == 0)
		{
			idString[5] = '\0';
			sn65dsi_read(sndev, 0x04, &idString[0]);
			sn65dsi_read(sndev, 0x03, &idString[1]);
			sn65dsi_read(sndev, 0x02, &idString[2]);
			sn65dsi_read(sndev, 0x01, &idString[3]);
			sn65dsi_read(sndev, 0x00, &idString[4]);

			if (strcmp((char *)idString, "DSI86") == 0)
			{
				printf("Attaching DP bridge %s\n", idString);
				do_enable_mipi2dp(dev, sndev);

				env_set("video_output_type", "DP");
				return;
			}
			else
			{
				printf("DP bridge read error - %s\n", idString);
			}
		}
	}

	printf("No bridge found, attaching MIPI-DSI panel\n");
	do_enable_mipi_dsi_panel(dev);
	env_set("video_output_type", "MIPI-DSI");
}


struct display_info_t displays[] =
{
	{
		.bus = LCDIF_BASE_ADDR,
		.addr = 0,
		.pixfmt = 24,
		.detect = NULL,
		.enable	= do_enable_mipi_and_bridge,
		.mode	= { // is overwritten by the video autoconfig
			.name			= "1280x720",
			.refresh		= 60,
			.xres			= 1280,
			.yres			= 720,
			.pixclock		= 13468, /* 148500000 / 2 */
			.left_margin		= 220,	// HBP
			.right_margin		= 110,	// HFP
			.upper_margin		= 20,	// VBP
			.lower_margin		= 5,	// VFP
			.hsync_len		= 40,	// HSW
			.vsync_len		= 5,	// VSW
			.sync			= FB_SYNC_EXT,
			.vmode			= FB_VMODE_NONINTERLACED
		}
	},
};

size_t display_count = ARRAY_SIZE(displays);
#endif

int board_late_init(void)
{
#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif

	return 0;
}

#ifdef CONFIG_FSL_FASTBOOT
#ifdef CONFIG_ANDROID_RECOVERY
int is_recovery_key_pressing(void)
{
	return 0; /*TODO*/
}
#endif /*CONFIG_ANDROID_RECOVERY*/
#endif /*CONFIG_FSL_FASTBOOT*/
